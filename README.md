# Machine-learning

Bunch of project for learning machine learning & related topic 

## Projects
* `fastai` - project from "Deep Learning for Coders with fastai & PyTorch"
* `google-colab` - backup of Google Colab notebooks
* `gpt` - fine tune GPT2 using DPO and LoRA
* `huggingfface` - project from "NLP with Transformers and HuggingFace"
* `image-cluserization` - detecting number of classes in image dataset
* `python` - projects for learning python features & libraries
* `pytorch` - projects for learning pytorch
* `tf` - project for learning Tensorflow
* `titanic` - solution for [Titanic - Machine Learning from Disaster](https://www.kaggle.com/competitions/titanic/submissions)