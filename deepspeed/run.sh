#!/bin/bash
set -euo pipefail

export ACCELERATE_LOG_LEVEL="info"

export RUN_NO=1
export WANDB_NAME="DeepSpeed test ${RUN_NO}"

rm -rf data/*

accelerate launch \
  --config_file accelerate_config__deepspeed-3.yml \
  sft.py \
  2>&1 | tee "deepspeed_${RUN_NO}.log"
exit 0