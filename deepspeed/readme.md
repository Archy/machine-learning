# DeepSpeed training

https://huggingface.co/docs/accelerate/en/package_reference/accelerator

## Comparison:

Start from PyCharm:
```shell
***** train metrics *****
  epoch                    =        1.0
  total_flos               =    62890GF
  train_loss               =     2.8988
  train_runtime            = 0:01:17.04
  train_samples            =        128
  train_samples_per_second =      1.661
  train_steps_per_second   =      0.831

***** eval metrics *****
  epoch                   =        1.0
  eval_loss               =     2.6602
  eval_runtime            = 0:00:03.23
  eval_samples            =         16
  eval_samples_per_second =      4.941
  eval_steps_per_second   =       2.47
```

DeepSpeed (1 gpu):
```shell
***** train metrics *****
  epoch                    =        1.0
  total_flos               =    62890GF
  train_loss               =     2.8985
  train_runtime            = 0:01:21.32
  train_samples            =        128
  train_samples_per_second =      1.574
  train_steps_per_second   =      0.787

***** eval metrics *****
  epoch                   =        1.0
  eval_loss               =     2.6602
  eval_runtime            = 0:00:03.30
  eval_samples            =         16
  eval_samples_per_second =      4.837
  eval_steps_per_second   =      2.418
```

### sanna.kask

```shell
***** train metrics *****
  epoch                    =        1.0
  total_flos               =    62890GF
  train_loss               =      2.847
  train_runtime            = 0:00:52.46
  train_samples            =        128
  train_samples_per_second =       2.44
  train_steps_per_second   =       1.22
                  
***** eval metrics *****
  epoch                   =        1.0
  eval_loss               =      2.627
  eval_runtime            = 0:00:00.86
  eval_samples            =         16
  eval_samples_per_second =     18.557
  eval_steps_per_second   =      9.279
```

```shell
***** train metrics *****
  epoch                    =        1.0
  total_flos               =    62890GF
  train_loss               =     2.8658
  train_runtime            = 0:00:27.87
  train_samples            =        128
  train_samples_per_second =      4.592
  train_steps_per_second   =      1.148
                   
***** eval metrics *****
  epoch                   =        1.0
  eval_loss               =      2.625
  eval_runtime            = 0:00:00.54
  eval_samples            =         16
  eval_samples_per_second =     29.565
  eval_steps_per_second   =      7.391
```