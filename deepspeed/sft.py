import os
import traceback
from pathlib import Path
from typing import Optional

import torch
from datasets import load_dataset
from huggingface_hub import ModelCardData, ModelCard, RepoCard
from peft import LoraConfig, get_peft_model
from pkg_resources import resource_filename
from transformers import AutoModelForCausalLM, AutoTokenizer, set_seed, BitsAndBytesConfig
from trl import SFTConfig, SFTTrainer, setup_chat_format
from trl.trainer import ConstantLengthDataset

chat_template_example = """{%- for message in messages %}
{{- '<|im_start|>' + message['role'] + '\n' + message['content'] + '<|im_end|>' + '\n' }}
{%- endfor %}"""

dataset_name = 'HuggingFaceH4/ultrachat_200k'

def _format_text(tokenizer, example):
    text = tokenizer.apply_chat_template(example['messages'], tokenize=False)
    return text


def _estimate_chars_per_token(dataset, tokenizer, examples):
    total_characters, total_tokens = 0, 0
    i = 0
    for example in dataset:
        text = _format_text(tokenizer, example)
        total_characters += len(text)
        if tokenizer.is_fast:
            total_tokens += len(tokenizer(text).tokens())
        else:
            total_tokens += len(tokenizer.tokenize(text))

        i += 1
        if i > examples:
            break

    return total_characters / total_tokens


def _load_dataset(file_path, split):
    return load_dataset('json', data_files=file_path, field=split, cache_dir=None)['train']


def _prepare_datasets(tokenizer, seq_length, limit_dataset):
    # train_dataset = _load_dataset(dataset_path, 'train')
    # eval_dataset = _load_dataset(dataset_path, 'validation')

    train_dataset = load_dataset(dataset_name, split='train_sft')
    eval_dataset = load_dataset(dataset_name, split='test_sft')
    if limit_dataset:
        print('Limiting dataset size')
        train_dataset = train_dataset.select(range(128))
        eval_dataset = eval_dataset.select(range(16))

    chars_per_token = _estimate_chars_per_token(train_dataset, tokenizer, 100)
    print(f'The character per token ratio is: {chars_per_token:.2f}')

    train_dataset = ConstantLengthDataset(
        tokenizer,
        train_dataset,
        formatting_func=lambda example: _format_text(tokenizer, example),
        infinite=False,
        seq_length=seq_length,
        chars_per_token=chars_per_token,
    )
    eval_dataset = ConstantLengthDataset(
        tokenizer,
        eval_dataset,
        formatting_func=lambda example: _format_text(tokenizer, example),
        infinite=False,
        seq_length=seq_length,
        chars_per_token=chars_per_token,
    )
    return train_dataset, eval_dataset


def train_process(
        model_name: str,
        chat_template: str | None,
        training_args: SFTConfig,
        lora_config: LoraConfig,
        output_dir: str,
        limit_dataset: bool,
):
    """SFT training function"""
    try:
        print(f'Training process {os.getpid()} started', flush=True)
        # accelerator = Accelerator()
        # if accelerator.is_main_process:
        #     print(f'I am main process!!!')
        # print(f'DONE')
        # exit(0)
        set_seed(training_args.seed)

        print('Loading tokenizer')
        tokenizer = AutoTokenizer.from_pretrained(model_name)

        print('Loading model')
        quantization_config = BitsAndBytesConfig(
            # load_in_8bit=True,
            load_in_4bit=True,
            bnb_4bit_compute_dtype=torch.float16
        )

        model = AutoModelForCausalLM.from_pretrained(model_name,
                                                     return_dict=True,
                                                    #  device_map="auto",
                                                     quantization_config=quantization_config)
        print('Model loaded')
        print('Model modules:')
        print(next(model.modules()))

        model = get_peft_model(model, lora_config)
        model.config.use_cache = False
        print('LoRA adapter created')

        if chat_template is not None:
            if tokenizer.chat_template:
                raise Exception(f'Tokenizer {model_name} already has a chat template. Chaning it is not supported.')

            if "<|im_start|>" in chat_template:
                print('Detected ChatML template format. Adding new tokens to the tokenizer')
                model, tokenizer = setup_chat_format(model, tokenizer)
            else:
                # TODO: correctly handle other formats
                tokenizer.chat_template = chat_template
        elif tokenizer.chat_template is None:
            print(f'No chat template provided, and model {model_name} does not have existing chat template')

        if not tokenizer.pad_token:
            tokenizer.pad_token = tokenizer.eos_token
        tokenizer.padding_side = 'right'

        print('Loading datasets')
        train_dataset, eval_dataset = _prepare_datasets(tokenizer, training_args.max_seq_length, limit_dataset)
        print('Dataset loaded')

        trainer = SFTTrainer(
            model,
            tokenizer=tokenizer,
            train_dataset=train_dataset,
            eval_dataset=eval_dataset,
            args=training_args,
        )

        print('Starting training')
        train_result = trainer.train()
        print('Training done')

        metrics = train_result.metrics
        metrics["train_samples"] = len(train_dataset)
        trainer.log_metrics("train", metrics)
        trainer.save_metrics("train", metrics)
        trainer.log_
        print('Training done. Saving model...')


        trainer.save_state()
        if trainer.accelerator.is_main_process:
            trainer.create_model_card(
                model_name=f'{model_name}__lora_adapter',
                dataset_name=dataset_name,
                tags="caise",
            )
            trainer.model.config.use_cache = True
            model.save_pretrained(output_dir)

        if training_args.do_eval:
            print("*** Evaluate ***")
            metrics = trainer.evaluate()
            metrics["eval_samples"] = len(eval_dataset)
            trainer.log_metrics("eval", metrics)
            trainer.save_metrics("eval", metrics)

    except Exception as e:
        print(f'Exception occured: {e}')
        traceback.print_exc()
        exit(0)


def train_gpt2():
    work_dir = Path(__file__).parent
    output_dir = work_dir / 'data'
    model_name = 'openai-community/gpt2'

    sft_config = SFTConfig(
        max_seq_length=1024,
        num_train_epochs=1,
        per_device_train_batch_size=2,
        per_device_eval_batch_size=2,
        gradient_accumulation_steps=1,
        gradient_checkpointing=False,
        optim='adamw_torch',
        weight_decay=0.0,
        seed=42,
        learning_rate=5e-5,
        lr_scheduler_type='cosine',
        warmup_ratio=0.01,
        eval_strategy='steps',
        eval_steps=10,
        do_eval=True,
        save_strategy='steps',
        save_steps=10,
        save_total_limit=1,
        logging_strategy='steps',
        logging_steps=10,
        report_to=['wandb'],
        run_name='test',
        push_to_hub=False,
        bf16=False,
        output_dir=str(output_dir),
        packing=True,
        label_names=['labels']
    )
    lora_config = LoraConfig(
        r=8,
        lora_alpha=16,
        lora_dropout=0.05,
        bias="none",
        task_type="CAUSAL_LM",
        target_modules=['c_attn', 'c_proj'],
    )

    train_process(
        model_name=model_name,
        chat_template=chat_template_example,
        training_args=sft_config,
        lora_config=lora_config,
        output_dir=str(output_dir),
        limit_dataset=True,
    )

def train_llama32_1b():
    work_dir = Path(__file__).parent
    output_dir = work_dir / 'data'
    model_name = 'meta-llama/Llama-3.2-1B'

    sft_config = SFTConfig(
        max_seq_length=1024,
        num_train_epochs=1,
        per_device_train_batch_size=4,
        per_device_eval_batch_size=4,
        gradient_accumulation_steps=4,
        gradient_checkpointing=False, # doesn't work with deepspeed (but should)
        optim='adamw_torch',
        weight_decay=0.0,
        seed=42,
        learning_rate=5e-5,
        lr_scheduler_type='cosine',
        warmup_ratio=0.1,
        eval_strategy='steps',
        eval_steps=10,
        do_eval=True,
        save_strategy='steps',
        save_steps=10,
        save_total_limit=1,
        logging_strategy='steps',
        logging_steps=10,
        report_to=['wandb'],
        run_name='llama 3.2 1b - test',
        push_to_hub=False,
        bf16=False,
        output_dir=str(output_dir),
        packing=True,
        label_names=['labels']
    )
    lora_config = LoraConfig(
        r=16,
        lora_alpha=32,
        lora_dropout=0.05,
        bias="none",
        task_type="CAUSAL_LM",
        target_modules=['q_proj', 'k_proj', 'v_proj', 'o_proj', 'gate_proj', 'up_proj', 'down_proj',],
    )

    train_process(
        model_name=model_name,
        chat_template=chat_template_example,
        training_args=sft_config,
        lora_config=lora_config,
        output_dir=str(output_dir),
        limit_dataset=True,
    )


def from_template(
        card_data: ModelCardData,
        template_path: Optional[str] = None,
        template_str: Optional[str] = None,
        **template_kwargs,
):
    if template_path is None and template_str is None:
        template_path = resource_filename(
            ModelCard.__module__.split('.')[0],
            'templates/modelcard_template.md'
        )
    print('Function override', flush=True)
    return RepoCard.from_template(card_data, template_path, template_str, **template_kwargs)


if __name__ == '__main__':
    ModelCard.from_template = from_template

    assert torch.cuda.is_available(), 'CUDA not available'
    assert 'WANDB_API_KEY' in os.environ, 'WANDB_API_KEY is required'
    os.environ['WANDB_PROJECT'] = 'DeepSpeed'
    # train_gpt2()
    train_llama32_1b()
